# Based on https://github.com/kontena/rails5-demo

FROM ruby:alpine

ADD Gemfile /app/
ADD Gemfile.lock /app/

RUN apk --update add --virtual build-dependencies build-base ruby-dev openssl-dev libxml2-dev libxslt-dev \
    postgresql-dev libc-dev linux-headers nodejs tzdata && \
    gem install bundler && \
    cd /app ; bundle install

ENV RAILS_ENV development
WORKDIR /app

CMD ["bundle", "exec", "rails", "s", "-p", "8080", "-b", "0.0.0.0"]
